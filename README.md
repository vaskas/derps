# derps

A simple github dependencies installer.

## Usage

Create a config file, e.g. `bundle.dep` with the following format.

```
ack              mileszs/ack.vim
ag               rking/ag.vim
gitv             gregsexton/gitv
nerdcommenter    scrooloose/nerdcommenter
coffee-script    kchmck/vim-coffee-script
color-jellybeans amacdougall/jellybeans.vim
powerline        Lokaltog/vim-powerline      develop
```

Run derps.

```
./derps bundle.dep
```

See the `bundle/` output directory. Directory name will match the dep file basename, so `cookbooks.dep` will correspond to `cookbooks` etc.
